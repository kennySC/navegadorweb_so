﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using tarea2_navegador.classes;


namespace tarea2_navegador
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>    
    public partial class MainWindow : Window
    {
        // Constante para acceder a la pagina de inicio
        static string INICIO = "https://www.google.com/";
        PestañasModelView pmv;  // Modelo de las pestañas
        WebBrowser wb_actual;   //  Webbrowser que nos utilizamos como el actual
        Pestaña p_actual;   //  pestaña que utilizamos como la pestaña actual
        List<Thread> hilos;

        public MainWindow()
        {            
            InitializeComponent();
            hilos = new List<Thread>();
            pmv = new PestañasModelView();
            tc_pestañas.ItemsSource = pmv.pestañas;
            crearPestaña();
        }

        public void crearPestaña()
        {
            Console.WriteLine("creando Pestaña");
            p_actual = new Pestaña();
            wb_actual = new WebBrowser();
            p_actual.contenido = wb_actual;
            p_actual.Encabezado = "Cargando";
            pmv.pestañas.Add(p_actual);
            wb_actual.Source = new Uri(INICIO);
            wb_actual.LoadCompleted += Wb_actual_LoadCompleted;
            wb_actual.Navigating += Wb_actual_Navigating;
        }

        private void Wb_actual_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            p_actual.Encabezado = "Cargando";
        }

        private void Wb_actual_LoadCompleted(object sender, NavigationEventArgs e)
        {
            dynamic doc = wb_actual.Document; //tabBrowser is the name of WebBrowser Control            
            Console.WriteLine(doc.Title);
            p_actual.Encabezado = doc.Title;
        }

        private void ir_atras(object sender, RoutedEventArgs e)
        {
            try
            {
                p_actual = (Pestaña)tc_pestañas.SelectedContent;
                wb_actual = (WebBrowser)p_actual.contenido;
                if (wb_actual.CanGoBack)
                {
                    wb_actual.GoBack();
                    Console.WriteLine(p_actual.Encabezado);
                }
            }
            catch (Exception) { }
        }

        private void ir_adelante(object sender, RoutedEventArgs e)
        {
            try
            {
                p_actual = (Pestaña)tc_pestañas.SelectedContent;
                wb_actual = (WebBrowser)p_actual.contenido;
                if (wb_actual.CanGoForward)
                {
                    wb_actual.GoForward();
                }

            }
            catch (Exception) { }
        }

        private void ir_Inicio(object sender, RoutedEventArgs e)
        {
            try
            {
                p_actual = (Pestaña)tc_pestañas.SelectedContent;
                wb_actual = (WebBrowser)p_actual.contenido;
                wb_actual.Source = new Uri(INICIO);
            }
            catch (Exception) { }
        }

        private void recargar(object sender, RoutedEventArgs e)
        {
            try
            {
                p_actual = (Pestaña)tc_pestañas.SelectedContent;
                wb_actual = (WebBrowser)p_actual.contenido;
                wb_actual.Refresh();
            }
            catch (Exception) { }
        }

        private void nuevaPestana(object sender, RoutedEventArgs e)
        {
            crearPestaña();
        }

        private void cerrarPestaña(object sender, RoutedEventArgs e)
        {            
            if(sender is Button btn && btn.Tag is TabItem item)
            {
                Pestaña p = (Pestaña)item.Content;          
                if(p != null)
                {
                    if (pmv.pestañas.Count() == 1)
                    {
                        crearPestaña();
                    }                                                    
                    pmv.pestañas.Remove(p);
                }
            }                       

        }

        private void ir_buscar(object sender, RoutedEventArgs e)
        {
            p_actual = (Pestaña)tc_pestañas.SelectedContent;
            wb_actual = (WebBrowser)p_actual.contenido;
            wb_actual.Navigate("https://" + txt_direccion.Text);
        }
    }
}
