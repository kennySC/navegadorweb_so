﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using tarea2_navegador.classes;


namespace tarea2_navegador
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>    
    public partial class MainWindow : Window
    {
        // Constante para acceder a la pagina de inicio
        static string INICIO = "https://www.google.com/";
        static int MAX_PESTAÑAS = 6;
        bool inPrograma;
        PestañasModelView pmv;  // Modelo de las pestañas        
        List<Thread> hilos;
        Historial hist;
        ObjectCache cache = MemoryCache.Default;
   
        public MainWindow()
        {            
            InitializeComponent();
            inPrograma = true;
            hilos = new List<Thread>();
            pmv = new PestañasModelView();
            hist = new Historial();
            tc_pestañas.ItemsSource = pmv.pestañas;
            crearPestaña();
        }

        //  Metodo que crea una pestaña en un nuevo hilo
        public void crearPestaña()
        {
            //  Primero creamos el hilo;
            Thread h = new Thread(() =>
            {
                Thread.CurrentThread.Name = "Pestaña" + (tc_pestañas.Items.Count + 1);
                Pestaña pes = new Pestaña(); // creamos la variable de tipo pestaña
                //  En un dispatcher realizamos la asignacion con variables de UI o procedentes del hilo principal
                Dispatcher.Invoke(new Action(() => {                                        
                    WebBrowser wb = new WebBrowser(); // creamos el navegador que ira adentro de la pestaña
                    pes.navegador = wb; // se lo asingamos a la pestaña
                    pes.Encabezado = "Cargando"; // seteamos el encabezadod e la pestaña como cargando, hasta que termine de cargar la pagina
                    pes.cerrar = false; // evitamos el cierre de la pestaña
                    pmv.pestañas.Add(pes); // la añadimos a la lista de ventanas abiertas
                    wb.Source = new Uri(INICIO); // Seteamos el inicio del navegador en google.com
                    wb.LoadCompleted += wb_loadCompleted; // Asignamos los eventos de loadCompleted para cuando se termina de cargar la pagina web
                    wb.Navigating += wb_Navigating; //  Asignamos el evento de Navigating para antes de navegar a la nueva direccion web
                    tc_pestañas.SelectedIndex = tc_pestañas.Items.Count - 1; // Seteamos la nueva pestaña como la pestaña seleccionada
                }), DispatcherPriority.Render); // terminamos el dispatcher con prioridad de Render
                // Abrimos un while para evitar que el hilo muera
                // que nor servira para eliminar el hilo al cerrar la pestaña
                while (!pes.cerrar && inPrograma) 
                {
                    Thread.Sleep(100); // lo hacemos dormir por 100 ms
                }
               
            });
            h.SetApartmentState(ApartmentState.STA);
            h.Start();
        }

        /*      IMPORTANTE !!!!!!!!!!
                    Metodo de Navigating para cuando el navegador actual ha terminado de cargar 
                    la pagina web.
                    Necesario para:
                        - Setear el nombre de la pagina en la pestaña
                        - Guardar paginas visitadas en el historial
                        - Guardar Paginas en la Cache                               */
        private void wb_Navigating(object sender, NavigatingCancelEventArgs e)
        {            
            Pestaña pes = (Pestaña)tc_pestañas.SelectedContent;
            pes.Encabezado = "Cargando";
            txt_direccion.Text = pes.navegador.Source.AbsoluteUri;
            
        }

        /* IMPORTANTE !!!!!!!!!!
                Metodo de loadCompleted para cuando el navegador actual ha terminado de cargar 
                la pagina web.
                Necesario para:
                    - Setear el nombre de la pagina en la pestaña
                    - Guardar paginas visitadas en el historial
                    - Guardar Paginas en la Cache */
        private void wb_loadCompleted(object sender, NavigationEventArgs e)
        {
            
            if(sender is WebBrowser wb)
            {
                string Cachekey = wb.Source.ToString();
                if(!cache.Contains(Cachekey) && !Cachekey.Contains("search"))
                {
                    var Document = wb.Document;

                    CacheItemPolicy policy = new CacheItemPolicy();
                    policy.AbsoluteExpiration = DateTime.Now.AddHours(1.0);
                    cache.Add(Cachekey, Document, policy);
                }
                hist.agregar_direccion(txt_direccion.Text, DateTime.Now);
                try
                {                    
                    Pestaña pes = (Pestaña)tc_pestañas.SelectedContent;
                    pes.Encabezado = (string)wb.InvokeScript("eval", "document.title.toString()");
                    //pes.Encabezado = doc.Title;                    
                }
                catch (Exception) {}
            }            
        }

        //  Metodo para ir a la pagina anterior, de ser posible, en el navegador actual
        private void ir_atras(object sender, RoutedEventArgs e)
        {
            try
            {
                Pestaña pes = (Pestaña)tc_pestañas.SelectedContent; // Obtenemos la pestaña casteando desde el contenido del item seleccionado en el TabControl                 
                if (pes.navegador.CanGoBack)
                {
                    pes.navegador.GoBack();                    
                }
            }
            catch (Exception) { }
        }

        //  Metodo para ir a la pagina siguiente, de ser posible, en el navegador actual
        private void ir_adelante(object sender, RoutedEventArgs e)
        {
            try
            {
                Pestaña pes = (Pestaña)tc_pestañas.SelectedContent;   // Obtenemos la pestaña casteando desde el contenido del item seleccionado en el TabControl             
                if (pes.navegador.CanGoForward) // Si puede ir 
                {
                    pes.navegador.GoForward(); // Va xdxd
                }

            }
            catch (Exception) { }
        }

        //  Evento de control para ir al inicio en el WebBrowser actual
        private void ir_Inicio(object sender, RoutedEventArgs e)
        {
            try
            {
                Pestaña pes = (Pestaña)tc_pestañas.SelectedContent; // Obtenemos la pestaña casteando desde el contenido del item seleccionado en el TabControl
                pes.navegador.Source = new Uri(INICIO);
            }
            catch (Exception) { }
        }

        //  Evento de control para recargar el WebBrowser actual
        private void recargar(object sender, RoutedEventArgs e)
        {
            try
            {
                Pestaña pes = (Pestaña)tc_pestañas.SelectedContent;  // Obtenemos la pestaña casteando desde el contenido del item seleccionado en el TabControl
                pes.navegador.Refresh();
            }
            catch (Exception) { }
        }

        //  Evento del boton que agrega una nueva pestaña
        private void nuevaPestana(object sender, RoutedEventArgs e)
        {
            crearPestaña(); // Llamamos al metodo crearPestaña que es un metodo aparte ya que necesita ser llamado fuera de eventos tambien y no puede depender de un sender
        }

        //  Metodo para cerrar una pestaña 
        private void cerrarPestaña(object sender, RoutedEventArgs e)
        {            
            if(sender is Button btn && btn.Tag is TabItem item) // Preguntamos si el evento viene desde un boton y si el tag de ese boton es un TabItem
            {
                Pestaña p = (Pestaña)item.Content; // Obtenemos la pestaña casteando desde el item que viene del TabControl como tag del boton
                if(p != null)
                {
                    p.navegador.Dispose(); // Liberamos los recursos usados por el navegador
                    p.cerrar = true; // Terminamos el hilo de la pestaña
                    if (pmv.pestañas.Count() == 1)  // Si la pestaña por cerrar es la unica abierta creamos una nueva pestaña
                    {
                        crearPestaña();
                    }                                          
                    pmv.pestañas.Remove(p); // Luego la removemos del array de pestañas
                }
            }                       

        }

        // Metodo que utiliza la direccion ingresada en la barra de direcciones para acceder a una nueva direccion
        private void ir_buscar(object sender, RoutedEventArgs e)
        {
            Pestaña pes = (Pestaña)tc_pestañas.SelectedContent; // Obtenemos la pestaña casteando desde el contenido del item seleccionado en el TabControl                        
            pes.navegador.Navigate("https://"+txt_direccion.Text);            
        }

        private void opciones(object sender, RoutedEventArgs e)
        {
            int i = 0;
            Console.WriteLine("Historial: \n\n");
            foreach (Pagina p in hist.direcciones)
            {
                Console.WriteLine("Entrada " + i + ": ");                
                Console.WriteLine("\tDirecicon: " + p.URL.ToString());
                Console.WriteLine("\tFecha: " + p.fecha + "\n\n");
                i++;
            }
        }

        // Metodo que se encarga de terminar los hilos que queden abiertos al cerrar la aplicacion
        private void cerrando(object sender, System.ComponentModel.CancelEventArgs e)
        {
            hist.almacenar_direcciones(hist.getDirecciones());
            inPrograma = false;
            foreach(Thread h in hilos) { h.Abort(); }
        }
    }
}
