﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tarea2_navegador.classes
{
    class Pagina
    {
        public Pagina(String url, DateTime fecha)
        {
            this.URL = url;
            this.fecha = fecha;
        }

        public string URL { set; get; } // La direccion de la pagina.
        // public string Title { set; get; } No se si sea necesario la dvd
        public DateTime fecha { set; get; } // La fecha en la que el usuario ingreso a la pagina.
    }
}
