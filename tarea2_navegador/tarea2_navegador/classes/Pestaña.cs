﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace tarea2_navegador.classes
{
    class Pestaña: INotifyPropertyChanged
    {   
        // titulo de la pestaña a mostrar
        private string encabezado;
        // Contenido de la pestaña
        public WebBrowser navegador { set; get; }
        // Utilizamos esta variable para terminar los hilos de las pestañas
        public bool cerrar { set; get; }

        public event PropertyChangedEventHandler PropertyChanged;

        public string Encabezado
        {
            get { return encabezado; }
            set
            {
                encabezado = value;
                OnPropertyChanged();
            }
        }        

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
