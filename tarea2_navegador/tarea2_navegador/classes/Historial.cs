﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.IO;
using System.Xml;
using System.Windows.Threading;

namespace tarea2_navegador.classes
{
    class Historial
    {
         public Historial()
        {   
        }
        
        String dir_archivo = "HISTORIAL.xml"; // Direccion dentro del proyecto 'tarea2_navegador\bin\Debug\PRUEBA.xml' para almacenar el historial, hay que cambiarle el nombre obvio
        public Pagina pagina;
        public List<Pagina> direcciones = new List<Pagina>(); // Lista de direcciones que se va a llenar mientras el programa/proyecto este corriendo

        // Metodo para agregar una pagina visitada al arreglo/lista de direcciones para el historial //
        public void agregar_direccion(String url, DateTime fecha)
        {   
            if(url != "" && url!= null ){           
                direcciones.Add(new Pagina(url, fecha));
            }
           
        }

        public List<Pagina> getDirecciones(){
            return direcciones;
        }

        // Metodo para guardar la lista llena de paginas visitadas en un archivo XML //
        public void almacenar_direcciones(List<Pagina> direcciones)
        {            
            File.Create(dir_archivo); // En caso de no existir, la creamos                
            try
            {
                XmlWriterSettings myWriterSettings = new XmlWriterSettings();
                myWriterSettings.CloseOutput = true;
                myWriterSettings.NewLineOnAttributes = true;
                myWriterSettings.Indent = true;
                myWriterSettings.Encoding = Encoding.UTF8;
                using (XmlWriter historial = XmlWriter.Create(dir_archivo, myWriterSettings))
                {// Creamos un escritor XML                    
                    historial.WriteStartDocument(); // Iniciamos el documento XML
                    historial.WriteComment("Historial de paginas visitadas");
                    historial.WriteStartElement("PaginasVisitadas"); // Iniciamos un elemento "global", como un grupo digamos, en este caso todas las paginas visitadas
                    foreach (Pagina p in direcciones)
                    {
                        historial.WriteStartElement("Pagina"); // Elementos por individual, cada pagina es un elemento: "Pagina"
                        historial.WriteElementString("Fecha", p.fecha.ToString());
                        historial.WriteElementString("URL", p.URL.ToString());
                        historial.WriteEndElement(); // Cerramos el elemento de pagina
                    }
                    historial.WriteEndElement(); // Cerramos el elemento/lista que engloba todas las paginas visitadas
                    historial.WriteEndDocument(); // Se cierra el documento que abrimos 
                    historial.Flush();
                    historial.Close();
                }
            }
            catch (UnauthorizedAccessException)
            {
                //throw;
            }
        }

        public void obtener_direcciones(){ // Metodo en el cual leemos el archivo XML donde se encuentran todas las paginas visitadas, es decir, el historial
            try{
                XmlDocument doc = new XmlDocument(); // Creamos un documento XML 
                doc.Load(dir_archivo); // Para cargar el que ya tenemos creado, el historial 
                foreach(XmlNode nodo in doc.DocumentElement){ // Consultamos por cada uno de los nodos(paginas en el caso de nosotros) dentro del XML
                    Console.WriteLine("Penis");
                }
            }
            catch(Exception e){
                MessageBox.Show(e.StackTrace);
            }

        }

    }
}
