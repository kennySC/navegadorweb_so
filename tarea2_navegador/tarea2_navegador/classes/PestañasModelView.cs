﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace tarea2_navegador.classes
{
    class PestañasModelView
    {
        public ObservableCollection<Pestaña> pestañas { get; set; }

        public PestañasModelView()
        {
            pestañas = new ObservableCollection<Pestaña>();
        }

    }
}
